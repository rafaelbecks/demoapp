demoApp.controller("loginController",function($scope,$base_services,$state,$ionicPopup,$ionicLoading){

	$scope.login=function(credenciales){
		 $ionicLoading.show({
		      template: '<ion-spinner icon="android"></ion-spinner>'
		    });
		$base_services.login(credenciales).then(function(data){
			localStorage.userData=JSON.stringify(data.data.mensaje);
			$state.go('home');
			$ionicLoading.hide();
		});
	}
});

demoApp.controller('homeController',function($scope,$base_services,$state){
    
    if(localStorage.userData==undefined || localStorage.userData=='')
    {
        $state.go("main");
    }

    $scope.usuario = JSON.parse(localStorage.userData).usuario[0];

    $scope.cerrarSesion = function()
    {
        localStorage.userData = undefined;
        $state.go("main");
    }
});
