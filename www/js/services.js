demoApp.factory('$base_services', function($http,$ionicPopup,$ionicLoading){
	var obj={};
//	var serviceBase="http://127.0.0.1:3000/";
	var serviceBase="http://107.170.102.121/adherenzien-dev/src/"
	var REQUEST_HEADERS = {
		API_KEY_VALUE:'az2015abcde',
		CONTENT_TYPE_VALUE: 'application/json'
	};


	obj.login=function(credenciales){
		var request={
				method : "POST",
	        	url : serviceBase+'security/access_token',
	        	headers: {
	        		'Content-Type': REQUEST_HEADERS.CONTENT_TYPE_VALUE,
	        		'X-Az-API-Key': REQUEST_HEADERS.API_KEY_VALUE
	        	},
		   data:credenciales
		 }
		 return $http(request).success(function () {
		 	console.log("ok");
		})
		 .error(function (data) {
			$ionicLoading.hide();
		 	$ionicPopup.alert({
			     title: 'Error!',
			     template: '<center>Credenciales Incorrectas</center>'
			   });
		});
	};

	return obj;
});