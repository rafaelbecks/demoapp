if(document.URL.indexOf("#")==-1){
  location.href="#/";
}

var demoApp=angular.module('demoApp', ['ionic','ui.router']);

demoApp.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

demoApp.config(function($stateProvider) {
        $stateProvider
            .state('main', {
                url: '/',
                templateUrl : 'login.html',
                controller  : 'loginController',
                cache:false
            })
            .state('home',{
            	url:'/home',
            	templateUrl: 'home.html',
            	controller: 'homeController',
              cache:false
            })
});


